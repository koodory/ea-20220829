# 12. version-control-system.md

Date: 2023-04-10

## Status

Accepted

## Context

TTA 실습에 필요한 기본 환경 배포를 무려로 하려고 한다. github이 더 많은 사용자를 가지고 있지만, gitlab은 오픈소스여서 나중에 on-premise로 이전 하기 쉬울 것 같다.

## Decision

버전 관리 시스템을 gitlab으로 쓰겠다.

## Consequences

gitlab 계정을 만들고 이를 기반으로 개발관련 산출물을 관리한다.